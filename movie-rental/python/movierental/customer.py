from movie import REGULAR, NEW_RELEASE, CHILDRENS


class Customer:

    def __init__(self, name):
        self.name = name
        self.rentals = []

    def add_rental(self, rental):
        self.rentals.append(rental)

    def statement(self):
        totalAmount = 0.0
        frequentRenterPoints = 0

        # add header lines
        result = "Rental Record for " + self.name + "\n"

        for each in self.rentals:
            thisAmount = 0.0

            # determine amounts for each line
            if each.movie.price_code == REGULAR:
                thisAmount += 2
                if each.days_rented > 2:
                    thisAmount += (each.days_rented - 2) * 1.5
            elif each.movie.price_code == NEW_RELEASE:
                thisAmount += each.days_rented * 3
            elif each.movie.price_code == CHILDRENS:
                thisAmount += 1.5
                if each.days_rented > 3:
                    thisAmount += (each.days_rented - 3) * 1.5

            # add frequent renter points
            frequentRenterPoints += 1
            # add bonus for a two day new release rental
            if each.movie.price_code == NEW_RELEASE and each.days_rented > 1:
                frequentRenterPoints += 1

            # show figures for this rental
            result += "\t" + each.movie.title + "\t" + str(thisAmount) + "\n"

            # compute total amount
            totalAmount += thisAmount

        # add footer lines
        result += "Amount owed is " + str(totalAmount) + "\n"
        result += "You earned " + str(frequentRenterPoints) + " frequent renter points"
        return result

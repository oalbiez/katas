package katas.transactions;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static java.time.Month.APRIL;
import static katas.transactions.Maximum.maximum;
import static org.junit.Assert.assertEquals;

public class RegrettableMaximumOfTest {
    MaximumOf stat;

    @Before
    public void setup() {
        stat = new MaximumOf();
    }

    @Test
    public void should_0() {
        assertEquals(Optional.empty(), stat.maximum());
    }

    @Test
    public void should_1() {
        Transaction t1 = Transaction.transaction("client", "A", 300, LocalDateTime.of(2017, APRIL, 17, 8, 0, 0));
        stat.updateWith(t1);
        assertEquals(Optional.of(maximum(300, LocalDateTime.of(2017, APRIL, 17, 8, 0, 0))), stat.maximum());
    }

    @Test
    public void should_2() {
        Transaction t1 = Transaction.transaction("client", "A", 300, LocalDateTime.of(2017, APRIL, 17, 8, 0, 0));
        Transaction t2 = Transaction.transaction("client", "A", 600, LocalDateTime.of(2017, APRIL, 17, 8, 30, 0));
        stat.updateWith(t1);
        stat.updateWith(t2);
        assertEquals(Optional.of(maximum(600, LocalDateTime.of(2017, APRIL, 17, 8, 30, 0))), stat.maximum());
    }

    @Test
    public void should_3() {
        Transaction t1 = Transaction.transaction("client", "A", 600, LocalDateTime.of(2017, APRIL, 17, 8, 0, 0));
        Transaction t2 = Transaction.transaction("client", "A", 300, LocalDateTime.of(2017, APRIL, 17, 8, 30, 0));
        stat.updateWith(t1);
        stat.updateWith(t2);
        assertEquals(Optional.of(maximum(600, LocalDateTime.of(2017, APRIL, 17, 8, 0, 0))), stat.maximum());
    }
}

package katas.transactions;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

import static java.time.Month.APRIL;
import static java.time.format.DateTimeFormatter.ISO_TIME;
import static java.util.Arrays.stream;
import static org.junit.Assert.assertEquals;

public class BetterMaximumOfTest {

    @Test
    public void should_0() {
        assertEquals("None", maximum());
    }

    @Test
    public void should_1() {
        assertEquals("300 at 08:00:00", maximum(transaction(300, LocalTime.of(8, 0, 0))));
    }

    @Test
    public void should_2() {
        assertEquals("600 at 08:30:00",
                maximum(
                        transaction(300, LocalTime.of(8, 0, 0)),
                        transaction(600, LocalTime.of(8, 30, 0))));
    }

    @Test
    public void should_3() {
        assertEquals("600 at 08:00:00",
                maximum(
                        transaction(600, LocalTime.of(8, 0, 0)),
                        transaction(300, LocalTime.of(8, 30, 0))));
    }

    private static Transaction transaction(final long amount, LocalTime when) {
        return Transaction.transaction("client", "A", amount, LocalDate.of(2017, APRIL, 17).atTime(when));
    }

    private static String maximum(final Transaction... transactions) {
        final MaximumOf stat = new MaximumOf();
        stream(transactions).forEach(stat::updateWith);
        return render(stat.maximum());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private static String render(final Optional<Maximum> maximum) {
        return maximum.map(m -> String.valueOf(m.amount()) + " at " + m.instant().toLocalTime().format(ISO_TIME)).orElse("None");
    }
}

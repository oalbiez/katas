package katas.transactions;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class Transaction {
    private final String client;
    private final String category;
    private final long amount;
    private final LocalDateTime creation;

    public static Transaction transaction(final String client, final String category, final long amount, final LocalDateTime creation) {
        return new Transaction(client, category, amount, creation);
    }

    private Transaction(final String client, final String category, final long amount, final LocalDateTime creation) {
        this.client = client;
        this.category = category;
        this.amount = amount;
        this.creation = creation;
    }

    public long amount() {
        return amount;
    }

    public LocalDateTime creation() {
        return creation;
    }

    public String render() {
        return "transaction(" + client + ", " + category + ", " + amount + ", " + creation.format(DateTimeFormatter.ISO_DATE_TIME) + ")";
    }
}

package katas.transactions;

import com.google.common.base.Objects;

import java.time.LocalDateTime;

public final class Maximum {
    public static final Maximum NONE = Maximum.maximum(Long.MIN_VALUE, LocalDateTime.MIN);

    private final long amount;
    private final LocalDateTime instant;

    public static Maximum maximum(long amount, LocalDateTime instant) {
        return new Maximum(amount, instant);

    }

    private Maximum(long amount, LocalDateTime instant) {
        this.amount = amount;
        this.instant = instant;
    }

    public long amount() {
        return amount;
    }

    public LocalDateTime instant() {
        return instant;
    }

    public Maximum combine(Maximum right) {
        return amount > right.amount() ? this : right;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Maximum that = (Maximum) o;
        return amount == that.amount &&
                Objects.equal(instant, that.instant);
    }

    public int hashCode() {
        return Objects.hashCode(amount, instant);
    }
}

package katas.transactions;

import java.util.Optional;

public final class MaximumOf {
    private Maximum maximum = Maximum.NONE;

    public void updateWith(final Transaction transaction) {
        maximum = maximum.combine(Maximum.maximum(transaction.amount(), transaction.creation()));
    }

    public Optional<Maximum> maximum() {
        return maximum == Maximum.NONE ? Optional.empty() : Optional.of(maximum);
    }
}

# Katas

This is a collection of code katas, dojo and other training exercies.


## Learning TDD

- coin-changer
- diamond
- fizzbuzz
- foobarqix
- lear-year
- mars-rover
- one-two
- potter
- prime-factors
- reversi
- rpn-calculator
- string-calculator
- tennis
- trading-card-game


## Double loop (TDD + Acceptance test)

- bowling-game
- game-of-life
- minesweeper


## Refactoring

- crc-refactoring
- glided-rose
- katastrophic
- movie-rental
- pharma-shopping
- quote-bot
- simwar
- space
- task-list
- telemtry-system
- text-converter
- tire-pressure-monitoring-system
- trip-service
- turn-ticker-dispenser


## Slicing

- carpaccio


## Conception

- bank-account
- basket-pricer
- birthday-greetings
- coffe-machine
- coffe-machine-project
- digisim
- invoice-reminder
- wallet

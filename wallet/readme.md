# Wallet


Given a Wallet containing Stocks, build a function that compute the value of wallet in a currency.
We have a wallet of stock values (different currencies).
We want to known the total value in a specific currency.
For rate exchange, you can use http://api.fixer.io/, google or yahoo.


## Notes

1. Each currency has it's own precision (EUR is 2, XBT is 8), value of given currency should then rounded accordingly
2. This kata has two differents parts :
  - wallet and stock, currency (training on calisthenic objects and SOLID)
  - rate exchange (training on testing external API)


## Extension

1. Pouvoir chercher le meilleur taux de change et être capable d'auditer les resultats

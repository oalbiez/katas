
# Trajectoire de vol

Ça pourrait être un truc comme ça :
Étant donné une trajectoire dont chacun des points sont exprimés en 4 dimensions (x,y,z et temps), et des volumes qui sont exprimés sous la forme de polygones 2D valides (pas de self intersection, polygone fermé) ayant une période de  validité dans le temps et une altitude ainsi qu'une élévation, construire une librairie capable de retourner tous les points d'intersection, entrée comme sortie, entre la trajectoire et un volume.

En bonus, considérer les points d'une trajectoire complètement à l'intérieur d'un volume comme étant une intersection, et rendre l'identification de ces points spécifiques possible.
